/**************************************************************************************************************************
 * This is the simplest possible code to connect to the renogy via RS232 and pull back a single piece of data. Real world
 * code would pass calculate CRCs, check for errors etc
 * 
 **************************************************************************************************************************/

void setup() {
  //the WEMOS D1 Mini has 1.5 serial ports. The "0.5" refers to the second serial port (Serial1) which only has a TX line.
  //we use Serial1 to send data to the Renogy. The regular serial port will receive data from the Renogy and can also
  //send and receive data from the USB port. This works well except that it is ambiguous if data is coming from the Renogy
  //or the USB port. Because of this we don't send data to the wemos via USB. We can, however, write data out of the USB
  //for debug information back to a PC. Because the renogy works at 9600 baud we need to set both ports to this speed.
  //if you are using a different device with only 1 serial port you can switch Serial1 to Serial in the readVoltage
  //function. This will mean the renogy will see the debug data we are writing to serial port but it does a decent job
  //of ignorning it.
  Serial.begin(9600);
  Serial1.begin(9600);
  delay(1000);
}

void loop() {
  // read the voltage every 2 seconds
  delay(2000);
  readVoltage();
}

void readVoltage() {
  //for the purpose of making the code as simple as possible everything is hard coded here.
  Serial1.write(0x01); //address, always 1 for RS232
  Serial1.write(0x03); //0x03 indicates we are reading register(s)
  Serial1.write(0x01); //register we wish to read, battery voltage, which is 0x0101. Most significant byte first
  Serial1.write(0x01); 
  Serial1.write(0x00); //count of values to read, just one in this case, MSB First
  Serial1.write(0x01);
  Serial1.write(0xd4); //CRC caclulated using https://crccalc.com/, LSB first
  Serial1.write(0x36);

  //read response from Serial port 0, allow 1 second for response to arrive
  int pos = 0;
  byte data[7];
  long startTime = millis();
  while(millis() < startTime + 1000) {
    while(Serial.available()) {
      data[pos] = Serial.read();
      pos++;
      if(pos >= 7) break;
    }
    if(pos >= 7) break;
  }

  //process response
  //we expect
  //0x01 - Device address
  //0x03 - Flag to say we are reading a register
  //0x02 - Length of data we are receiving. We requested 1 register but each register is 2 bytes
  //0x?? - Most significant 8 bits of voltage
  //0x?? - Least significant 8 bits of voltage
  //0x?? - Least significant 8 bits of CRC
  //0x?? - Most significant 8 bits of CRC. To keep code simple we don't check CRC here 
  if(data[0] == 0x01 && data[1] == 0x03 && data[2] == 0x02) {
    int voltage = ((int)data[3] << 8) | data[4];
    Serial.print("Voltage is ");
    Serial.println((float)voltage / 10.0);
  } else {
    Serial.print("Error reading voltage. Returned data: ");
    for(int i = 0; i < 7; i++) {
      if(i > 0) Serial.print(' ');
      if(data[i] < 16) Serial.print('0');
      Serial.print(data[i], HEX);
    }
    Serial.println();
  }
}
